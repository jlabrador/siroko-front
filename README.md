Prueba Técnica Siroko Frontend
========================

La prueba técnica del frontend de "Siroko" consiste en una pantalla con un listado de productos
que ofrece la api del backend. Se puede operar añadiendo productos al carrito de la compra con el
botón de 'add', eliminar con el de 'remove' y se puede cambiar el tamaño del número de los productos
del carrito directamente en el input del producto.

Para comprar el carrito solo debes de pulsar en el botón de buy cart y se resetearan todos los valores
a 0 con un alert indicando los productos que has comprado.

La prueba se ha con VUE 2.

Requerimientos
------------

  * node 16 o superior
  * npm 9.6

Instalación
------------

Instalar las dependencias del proyecto:

```bash
$ npm install
```


Ejecutar el entorno de desarrollo

```bash
$ npm run serve
```

Uso
-----

La aplicación será disponible a través de la url:

http://localhost:8080/

Contacto
-----

Cualquier duda de instalación o uso de la aplicación la puedes consultar en 
los siguientes contactos:

* Télefono: 655727029
* Correo: jose.labrador.gonzalez@gmail.com
